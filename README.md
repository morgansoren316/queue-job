# General Information

The job only prints info to the console. I focused only in the DevOps related tasks and getting the data from the queue. 

The Sevice Bus Queue usually deletes the messages from the queue after they were extracted. I had to make sure they are still there
because this is an scheduled job.

(Same information as the API)

### Image Build
there is a gitlab-ci.yaml, but it only builds for the  "latest" tag. the file build-image.sh generates an image tagged with the name of the branch. In this case, it will be release-0.0.1. That way I can have different images per release. 

### Helm Chart Installation
Since I dont have a helm chart repository, the file install-release.sh installs the chart directly from the repo. It installs it if the release does not exist, otherwise it just upgrades it. In case there are changes in the deployment, it will restart the the Pods. 

### Improvements
The keys to access the vault are hardcoded, in a Build Process tool like gitlab they can be stored as masked variables. 

The secret to access the container registry should also be added in the Build Process tool and replace the values in the chart.