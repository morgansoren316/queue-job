import os
from azure.servicebus import ServiceBusClient, ServiceBusMessage
from types import SimpleNamespace as Namespace
from azure.keyvault.secrets import SecretClient
from azure.identity import DefaultAzureCredential
import json

credential = DefaultAzureCredential()
client = SecretClient("https://moviesvaultsaeus.vault.azure.net/", credential=credential)

storage_secret = client.get_secret('storageAccount')
service_bus = client.get_secret('serviceBus')


storage = json.loads(storage_secret.value, object_hook=lambda d: Namespace(**d))
bus = json.loads(service_bus.value, object_hook=lambda d: Namespace(**d))

conn = f"Endpoint=sb://{bus.service_bus_name}/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey={bus.shared_access_key}"
queue_name = bus.queue_name
servicebus_client = ServiceBusClient.from_connection_string(conn_str=conn, logging_enable=True)

with servicebus_client:
    receiver = servicebus_client.get_queue_receiver(queue_name=bus.queue_name, max_wait_time=5, max_message_count=1)
    with receiver:
        for msg in receiver:
            print("Received: " + str(msg))