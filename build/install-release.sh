FILES=$(find '..' -type f \( -name "values.yaml" \))
    export BRANCH=$(git rev-parse --abbrev-ref HEAD)
    export BRANCH=$(echo "$BRANCH" | sed "s/\//-/g")

    for file in $FILES;do
        dos2unix $file
        sed -i 's/tag: .*/tag: '$BRANCH'/' $file
    	echo -e "$BLUE $file"
        echo -e "$BLUE $(<$file)"
    done

helm install --generate-name ../helm-chart/ -n movies-job

